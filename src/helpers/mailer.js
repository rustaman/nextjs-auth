import nodemailer from 'nodemailer';
import User from '@/models/userModel';
import bcryptjs from 'bcryptjs';

export const sendEmail = async ({ recipient, emailType, userId }) => {
  try {
    const hashedToken = await bcryptjs.hash(userId.toString(), 10);

    if (emailType === 'VERIFY') {
      await User.findByIdAndUpdate(userId, {
        verifyToken: hashedToken,
        verifyTokenExpiry: Date.now() + 3600000,
      });
    } else if (emailType === 'RESET') {
      await User.findByIdAndUpdate(userId, {
        forgotPasswordToken: hashedToken,
        forgotPasswordTokenExpiry: Date.now() + 3600000,
      });
    }

    const transport = nodemailer.createTransport({
      host: process.env.MAIL_SERVICE_URI,
      port: 2525,
      auth: {
        user: process.env.MAIL_SERVICE_USERNAME,
        pass: process.env.MAIL_SERVICE_PASSWORD,
      },
    });

    const mailOptions = {
      from: process.env.MAIL_SERVICE_SENDER_EMAIL,
      to: recipient,
      subject:
        emailType === 'VERIFY' ? 'Verify your account' : 'Reset your password',
      html: `<p>Click <a href="${
        process.env.domain
      }/verifyemail?token=${hashedToken}">here</a> to ${
        emailType === 'VERIFY' ? 'Verify your email' : 'reset your password'
      }.</p>`,
    };

    const mailReponse = await transport.sendMail(mailOptions);

    return mailReponse;
  } catch (error) {
    throw new Error(error.message);
  }
};
