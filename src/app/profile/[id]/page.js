const UserProfile = ({ params }) => {
  //console.log(params);
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <h1 className="text-6xl mb-4">Profile</h1>
      <hr />
      <p className="text-4xl">
        Profile page of user{' '}
        <span className="p-2 ml-2 rounded bg-green-500 text-black">
          {params.id}
        </span>
      </p>
    </div>
  );
};

export default UserProfile;
