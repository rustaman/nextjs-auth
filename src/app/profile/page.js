'use client';
import axios from 'axios';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'react-hot-toast';

const ProfilePage = () => {
  const router = useRouter();
  const [data, setData] = useState('');
  const logout = async () => {
    try {
      const response = await axios.get('/api/users/logout');
      if (response.status === 200) {
        toast.success(response.data.message);
        router.push('/login');
      }
    } catch (error) {
      console.log(error.message);
      toast.error(error.message);
    }
  };

  const getUserDetails = async () => {
    const res = await axios.get('/api/users/me');
    console.log(res.data);
    setData(res.data.data._id);
  };
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <h1 className="mb-4 text-3xl">Profile</h1>
      <hr />
      <p>Profile page</p>
      <h2 className="p-3 my-2 rounded bg-green-500">
        {data === '' ? (
          'No details yet'
        ) : (
          <Link href={`/profile/${data}`}>Profile page of user {data}</Link>
        )}
      </h2>
      <hr />
      <button
        onClick={getUserDetails}
        className="bg-green-700 hover:bg-green-800 text-white font-bold py-2 px-4 mb-2 rounded"
      >
        Get Details
      </button>
      <button
        onClick={logout}
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
      >
        Logout
      </button>
    </div>
  );
};

export default ProfilePage;
